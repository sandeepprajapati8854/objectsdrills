const mapObjects = require("../mapObject.cjs");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

function CallingCallback(val, items) {
  return val + 10;
}
let result = mapObjects(testObject, CallingCallback);
console.log(result);
