function values(obj) {
  if (typeof obj != "object") {
    return [];
  } else {
    let valueArray = [];
    for (let value in obj) {
      if (typeof obj[value] != "function") {
        valueArray.push(`${obj[value]}`);
      }
    }
    return valueArray;
  }
}
module.exports = values;
