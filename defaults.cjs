function defaults(obj, defaultsProp) {
  if (typeof obj != "object") {
    return [];
  } else {
    // let valueArray = [];
    for (let key in defaultsProp) {
      if (key in obj == false) {
        obj[key] = defaultsProp[key];
      }
    }
    return obj;
  }
}

module.exports = defaults;
