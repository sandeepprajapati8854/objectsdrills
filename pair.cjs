function pair(obj) {
  if (typeof obj != "object") {
    return [];
  } else {
    let valueArray = [];
    for (let value in obj) {
      //   if (typeof obj[value] != "function") {
      valueArray.push(`[${value},${obj[value]}]`);
      //   }
    }
    return valueArray;
  }
}
module.exports = pair;
