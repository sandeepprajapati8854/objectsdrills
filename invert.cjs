function invert(obj) {
  if (typeof obj != "object") {
    return [];
  } else {
    let returnObject = {};
    for (let key in obj) {
      returnObject[obj[key]] = key;
    }
    return returnObject;
  }
}
module.exports = invert;
