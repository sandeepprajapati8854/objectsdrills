function mapObjects(obj, CallingCallback) {
  if (typeof obj != "object") {
    return [];
  } else {
    for (let items in obj) {
      obj[items] = CallingCallback(obj[items]);
    }
    return obj;
  }
}

module.exports = mapObjects;
