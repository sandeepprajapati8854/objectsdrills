function keys(obj) {
  if (typeof obj != "object") {
    return [];
  } else {
    let keyArr = [];
    for (let key in obj) {
      keyArr.push(`${key}`);
    }
    return keyArr;
  }
}
module.exports = keys;
